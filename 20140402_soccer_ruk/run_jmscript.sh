

START_TIME=$(date +"%m-%d-%Y_%H-%M-%S")

JMETER_PATH="~/apache-jmeter-2.11/bin"

TEST_NAME=$1
TEST_DESC=$2
TEST_NAME_FILE="20140328_soccer_mguk1trading_10g"
JMX_FILE=${TEST_NAME}.jmx
LOG_FILE=${TEST_NAME}_${START_TIME}_${TEST_DESC}_${NUMBER}.log
JTL_FILE=${TEST_NAME}_${START_TIME}_${TEST_DESC}_${NUMBER}.jtl


echo "TEST_NAME = ${TEST_NAME}"
echo "JMX_FILE = ${JMX_FILE}"
echo "LOG_FILE = ${LOG_FILE}"
echo "JTL_FILE = ${JTL_FILE}"

CMD_STR="${JMETER_PATH}/jmeter.sh  -n -t ${JMX_FILE}  -j ${LOG_FILE} -l ${JTL_FILE}"
echo "Run command: ${CMD_STR}"

echo "git pull"
echo "git add ${JTL_FILE}"
echo "git commit -m '${JTL_FILE}' ${JTL_FILE}"
echo "git push"

date



