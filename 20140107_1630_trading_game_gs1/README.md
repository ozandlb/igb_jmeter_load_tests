### 2014-01-07 4:30pm
### Load test Game Client against `gs1` and Trading App against `bostage1`
### 50 games, 50 users, 50 threads, 45 loops -- test lasted approximately 60 minutes
=======

Heavier load test going beyond the previous tests to check configuration to test the limits of system capabilities.

Trading calls to `bostage1` seemed to respond without issues.

However, game client **place bet** calls to `gs1` generated very long response times.

Please see New Relic and JMeter response graphs and tables, included with this checkin.