
START_TIME=$(date +"%m-%d-%Y_%H-%M-%S")
TEST_DATE=$(date +"%m-%d-%Y")

JMETER_PATH="/usr/local/apache-jmeter-2.11/bin"

TEST_DESC=$1
TEST_NAME="dtrading_soccer_trading_30g"
JMX_FILE=jmx/${TEST_NAME}.jmx
LOG_FILE=logs/${TEST_DATE}/${TEST_NAME}_${START_TIME}_${TEST_DESC}_${NUMBER}.log
JTL_FILE=jtl/${TEST_DATE}/${TEST_DATE}/${TEST_NAME}_${START_TIME}_${TEST_DESC}_${NUMBER}.jtl


echo "TEST_NAME = ${TEST_NAME}"
echo "JMX_FILE = ${JMX_FILE}"
echo "LOG_FILE = ${LOG_FILE}"
echo "JTL_FILE = ${JTL_FILE}"
echo "mkdir jtl/${TEST_DATE} logs/${TEST_DATE} results/${TEST_DATE}"

CMD_STR="${JMETER_PATH}/jmeter.sh  -n -t ${JMX_FILE}  -j ${LOG_FILE} -l ${JTL_FILE}"
echo "Run command: "
echo "${CMD_STR}"

echo "##########"

echo "java -jar ${JMETER_PATH}/lib/ext/CMDRunner.jar --tool Reporter  --plugin-type AggregateReport  --input-jtl ${JTL_FILE}/${TEST_DATE}  --generate-csv results/aggregate_30_traders_${START_TIME}_${TEST_DESC}.csv"


#echo "git pull"
#echo "git add ${JTL_FILE}"
#echo "git commit -m '${JTL_FILE}' ${JTL_FILE}"
#echo "git push"

date
