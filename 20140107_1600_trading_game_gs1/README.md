### 2014-01-07 4pm Matching number of games and users to remove trading redundancies
### Load test Game Client against `gs1` and Trading App against `bostage1`
### 5 game, 5 users, 5 threads, 5 loops
=======

Load test routing **game client** calls to `gs1.betpump.com` and **trading calls** to `bostage1.betpump.com`.

Tests modified to match the number of games and users to remove redundant trading calls.

Tests to be redesigned so there is one trading action (e.g. opening a market) and subsequently several game client actions (placing bets on that market).