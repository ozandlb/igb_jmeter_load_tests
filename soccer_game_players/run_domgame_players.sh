

START_TIME=$(date +"%m-%d-%Y_%H-%M-%S")

JMETER_PATH="/usr/local/apache-jmeter-2.11/"
JMETER_BIN_PATH="${JMETER_PATH}/bin"

#TEST_NAME=$1
#TEST_DESC=$2
#NUMBER=$3
TEST_DESC=$1
NUMBER=$2
#TEST_NAME_FILE="20140901_stage2betpump_100u_slow"
#TEST_NAME_FILE="domgame_10u"
#TEST_NAME_FILE="domgame_100u"
#TEST_NAME_FILE="domgame_50u"
#TEST_NAME_FILE="domgame_100u2"
#TEST_NAME_FILE="domgame50u"
#TEST_NAME_FILE="20140402_soccer_rukgame_50u"
TEST_NAME_FILE="domgame_50u"
#JMX_FILE=jmx/${TEST_NAME}.jmx
JMX_FILE=jmx/${TEST_NAME_FILE}.jmx
LOG_FILE=logs/${TEST_NAME_FILE}_${START_TIME}_${TEST_DESC}_${NUMBER}.log
JTL_FILE=jtl/${TEST_NAME_FILE}_${START_TIME}_${TEST_DESC}_${NUMBER}.jtl


echo "TEST_NAME = ${TEST_NAME}"
echo "JMX_FILE = ${JMX_FILE}"
echo "LOG_FILE = ${LOG_FILE}"
echo "JTL_FILE = ${JTL_FILE}"

CMD_STR="${JMETER_BIN_PATH}/jmeter.sh  -n -t ${JMX_FILE}  -j ${LOG_FILE} -l ${JTL_FILE}"
echo "Run command: "
echo "${CMD_STR}"
echo "##########"

# echo "cp ${JTL_FILE} ~/perf/jtl"

#echo "java -jar ~/apache-jmeter-2.11/lib/ext/CMDRunner.jar --tool Reporter  --plugin-type AggregateReport  --input-jtl ~/perf/jtl/${JTL_FILE}  --generate-csv ~/perf/csv/aggregate_${TEST_DESC}_${NUMBER}.csv"

echo "java -jar ${JMETER_PATH}/lib/ext/CMDRunner.jar --tool Reporter  --plugin-type AggregateReport  --input-jtl ${JTL_FILE}  --generate-csv results/aggregate_${START_TIME}_${TEST_DESC}_${NUMBER}.csv"

#echo "git pull"
#echo "git add ${JTL_FILE}"
#echo "git commit -m '${JTL_FILE}' ${JTL_FILE}"
#echo "git push"

date



