### 2014-01-07 Work in Progress to calibrate multiple games, users and traders 
### Load test Game Client against gs1 and Trading App against bostage1
### 1 game, 10 users, 10 threads, 5 loops
=======

Work in progress load test designed to route **game client** calls to `gs1.betpump.com` and **trading calls** to `bostage1.betpump.com`.

Tests currently generating redundant Trading calls.  Way to avoid this for now is to have the same number of games as the number of users.

Tests will be redesigned so there is one trading action (e.g. opening a market) and subsequently several game client actions (placing bets on that market).
