# Load test 
###### 20-March-2014 
###### ~1:30pm Pacific

#### Soccer

#### Game
- All calls hitting `gs1` port 80
- 50 users
- 1 second ramp up time per thread
- 1 second loop timer delay
- unlimited loops (Blazemeter will terminate test after X minutes)

#### Trading
- All calls hitting `bostage1` port 8080
- 10 games
- 1 second ramp up time per thread
- 1 second loop timer delay
- unlimited loops (Blazemeter will terminate test after X minutes)
