### 2014-01-10 11:00 AM
### Load test both Game Client and Trading App and against `test1`
### 50 games, 50 users, 50 threads, 45 loops
=======

Test to investigate locks and high response times on GAME place bet call.

All calls seemed to respond normally, though the place bet call seemed to take approximately 4 times longer than other calls.

Please see New Relic and JMeter response graphs and tables, included with this checkin.